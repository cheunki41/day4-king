package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MarsRoverTest {

    @Test
    public void should_return_0_1_N_when_move_given_0_0_N() {
        // given
        Position position = new Position(0, 0, DirectionType.NORTH);
        MarsRover marsRover = new MarsRover(0, 0, DirectionType.NORTH);

        // when
        Position actual = marsRover.move(position);

        // then
        Position expected = new Position(0, 1, DirectionType.NORTH);
        assertEquals(expected, actual);
    }

    @Test
    public void should_return_1_0_E_when_move_given_0_0_E() {
        // given
        Position position = new Position(0, 0, DirectionType.EAST);
        MarsRover marsRover = new MarsRover(0, 0, DirectionType.EAST);

        // when
        Position actual = marsRover.move(position);

        // then
        Position expected = new Position(1, 0, DirectionType.EAST);
        assertEquals(expected, actual);
    }

    @Test
    public void should_return_0_0_S_when_move_given_0_1_S() {
        // given
        Position position = new Position(0, 1, DirectionType.SOUTH);
        MarsRover marsRover = new MarsRover(0, 1, DirectionType.SOUTH);

        // when
        Position actual = marsRover.move(position);

        // then
        Position expected = new Position(0, 0, DirectionType.SOUTH);
        assertEquals(expected, actual);
    }

    @Test
    public void should_return_0_0_W_when_move_given_1_0_W() {
        // given
        Position position = new Position(1, 0, DirectionType.WEST);
        MarsRover marsRover = new MarsRover(0, 0, DirectionType.WEST);

        // when
        Position actual = marsRover.move(position);

        // then
        Position expected = new Position(0, 0, DirectionType.WEST);
        assertEquals(expected, actual);
    }

    @Test
    public void should_return_0_0_W_when_turnLeft_given_0_0_N() {
        // given
        Position position = new Position(0, 0, DirectionType.NORTH);
        MarsRover marsRover = new MarsRover(0, 0, DirectionType.NORTH);

        // when
        Position actual = marsRover.turnLeft(position);

        // then
        Position expected = new Position(0, 0, DirectionType.WEST);
        assertEquals(expected, actual);
    }

    @Test
    public void should_return_0_0_S_when_turnLeft_given_0_0_W() {
        // given
        Position position = new Position(0, 0, DirectionType.WEST);
        MarsRover marsRover = new MarsRover(0, 0, DirectionType.WEST);

        // when
        Position actual = marsRover.turnLeft(position);

        // then
        Position expected = new Position(0, 0, DirectionType.SOUTH);
        assertEquals(expected, actual);
    }

    @Test
    public void should_return_0_0_E_when_turnLeft_given_0_0_S() {
        // given
        Position position = new Position(0, 0, DirectionType.SOUTH);
        MarsRover marsRover = new MarsRover(0, 0, DirectionType.SOUTH);

        // when
        Position actual = marsRover.turnLeft(position);

        // then
        Position expected = new Position(0, 0, DirectionType.EAST);
        assertEquals(expected, actual);
    }

    @Test
    public void should_return_0_0_N_when_turnLeft_given_0_0_E() {
        // given
        Position position = new Position(0, 0, DirectionType.EAST);
        MarsRover marsRover = new MarsRover(0, 0, DirectionType.EAST);

        // when
        Position actual = marsRover.turnLeft(position);

        // then
        Position expected = new Position(0, 0, DirectionType.NORTH);
        assertEquals(expected, actual);
    }

    @Test
    public void should_return_0_0_E_when_turnRight_given_0_0_N() {
        // given
        Position position = new Position(0, 0, DirectionType.NORTH);
        MarsRover marsRover = new MarsRover(0, 0, DirectionType.NORTH);

        // when
        Position actual = marsRover.turnRight(position);

        // then
        Position expected = new Position(0, 0, DirectionType.EAST);
        assertEquals(expected, actual);
    }

    @Test
    public void should_return_0_0_S_when_turnRight_given_0_0_E() {
        // given
        Position position = new Position(0, 0, DirectionType.EAST);
        MarsRover marsRover = new MarsRover(0, 0, DirectionType.EAST);

        // when
        Position actual = marsRover.turnRight(position);

        // then
        Position expected = new Position(0, 0, DirectionType.SOUTH);
        assertEquals(expected, actual);
    }

    @Test
    public void should_return_0_0_W_when_turnRight_given_0_0_S() {
        // given
        Position position = new Position(0, 0, DirectionType.SOUTH);
        MarsRover marsRover = new MarsRover(0, 0, DirectionType.SOUTH);

        // when
        Position actual = marsRover.turnRight(position);

        // then
        Position expected = new Position(0, 0, DirectionType.WEST);
        assertEquals(expected, actual);
    }

    @Test
    public void should_return_0_0_N_when_turnRight_given_0_0_W() {
        // given
        Position position = new Position(0, 0, DirectionType.WEST);
        MarsRover marsRover = new MarsRover(0, 0, DirectionType.WEST);

        // when
        Position actual = marsRover.turnRight(position);

        // then
        Position expected = new Position(0, 0, DirectionType.NORTH);
        assertEquals(expected, actual);
    }

    @Test
    public void should_return_0_1_N_when_executeCommand_given_M() {
        // given
        MarsRover marsRover = new MarsRover(0, 0, DirectionType.NORTH);

        // when
        String actual = marsRover.executeCommand("M");

        // then
        assertEquals("0 1 N", actual);
    }

    @Test
    public void should_return_0_0_E_when_executeCommand_given_R() {
        // given
        MarsRover marsRover = new MarsRover(0, 0, DirectionType.NORTH);

        // when
        String actual = marsRover.executeCommand("R");

        // then
        assertEquals("0 0 E", actual);
    }

    @Test
    public void should_return_0_0_W_when_executeCommand_given_L() {
        // given
        MarsRover marsRover = new MarsRover(0, 0, DirectionType.NORTH);

        // when
        String actual = marsRover.executeCommand("L");

        // then
        assertEquals("0 0 W", actual);
    }

    @Test
    public void should_return_neg_2_4_N_when_executeCommand_given_MMLMMRMM() {
        // given
        MarsRover marsRover = new MarsRover(0, 0, DirectionType.NORTH);

        // when
        String actual = marsRover.executeCommand("MMLMMRMM");

        // then
        assertEquals("-2 4 N", actual);
    }
}