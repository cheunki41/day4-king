package org.example;

public enum DirectionType {
    NORTH,
    EAST,
    SOUTH,
    WEST
}
