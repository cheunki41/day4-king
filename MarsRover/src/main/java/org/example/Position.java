package org.example;

import java.util.Objects;

public class Position {
    protected Integer coordinateX;
    protected Integer coordinateY;
    protected DirectionType direction;

    public Position(Integer coordinateX, Integer coordinateY, DirectionType direction) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
        this.direction = direction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return Objects.equals(coordinateX, position.coordinateX) && Objects.equals(coordinateY, position.coordinateY) && direction == position.direction;
    }

    @Override
    public int hashCode() {
        return Objects.hash(coordinateX, coordinateY, direction);
    }

    @Override
    public String toString() {
        String direction;
        switch (this.direction) {
            case NORTH:
                direction = "N";
                break;
            case SOUTH:
                direction = "S";
                break;
            case WEST:
                direction = "W";
                break;
            case EAST:
                direction = "E";
                break;
            default:
                direction = "";
        }
        return String.format("%d %d %s", coordinateX, coordinateY, direction);
    }
}
