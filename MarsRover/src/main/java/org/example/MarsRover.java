package org.example;

import java.util.Arrays;

public class MarsRover extends Position{

    public MarsRover(Integer coordinateX, Integer coordinateY, DirectionType direction) {
        super(coordinateX, coordinateY, direction);
    }

    public Position move(Position currentPosition) {
        switch(currentPosition.direction) {
            case NORTH:
                return new Position(currentPosition.coordinateX, currentPosition.coordinateY + 1, currentPosition.direction);
            case EAST:
                return new Position(currentPosition.coordinateX + 1, currentPosition.coordinateY, currentPosition.direction);
            case SOUTH:
                return new Position(currentPosition.coordinateX, currentPosition.coordinateY - 1, currentPosition.direction);
            case WEST:
                return new Position(currentPosition.coordinateX - 1, currentPosition.coordinateY, currentPosition.direction);
            default:
                return null;
        }
    }

    public Position turnLeft(Position currentPosition) {
        switch (currentPosition.direction) {
            case NORTH:
                return new Position(currentPosition.coordinateX, currentPosition.coordinateY, DirectionType.WEST);
            case WEST:
                return new Position(currentPosition.coordinateX, currentPosition.coordinateY, DirectionType.SOUTH);
            case SOUTH:
                return new Position(currentPosition.coordinateX, currentPosition.coordinateY, DirectionType.EAST);
            case EAST:
                return new Position(currentPosition.coordinateX, currentPosition.coordinateY, DirectionType.NORTH);
            default:
                return null;
        }
    }

    public Position turnRight(Position currentPosition) {
        switch (currentPosition.direction) {
            case NORTH:
                return new Position(currentPosition.coordinateX, currentPosition.coordinateY, DirectionType.EAST);
            case EAST:
                return new Position(currentPosition.coordinateX, currentPosition.coordinateY, DirectionType.SOUTH);
            case SOUTH:
                return new Position(currentPosition.coordinateX, currentPosition.coordinateY, DirectionType.WEST);
            case WEST:
                return new Position(currentPosition.coordinateX, currentPosition.coordinateY, DirectionType.NORTH);
            default:
                return null;
        }
    }

    public String executeCommand(String command) {
        Position reducePosition = Arrays.stream(command.split("")).reduce(new Position(coordinateX, coordinateY, direction), (accumulator, singleCommand) -> {
            switch (singleCommand) {
                case "M":
                    return move(accumulator);
                case "R":
                    return turnRight(accumulator);
                case "L":
                    return turnLeft(accumulator);
            }
            return null;
        }, (accumulator, position) -> position);
        coordinateX = reducePosition.coordinateX;
        coordinateY = reducePosition.coordinateY;
        direction = reducePosition.direction;
        return super.toString();
    }

}
